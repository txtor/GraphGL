#ifndef MESH_H
#define MESH_H
typedef unsigned int GLuint;
#include<glm/glm.hpp>

class Vertex{
public:
	Vertex(){}
	Vertex(const glm::vec3& pos)
	{
		this->pos = pos;
	}
	glm::vec3 GetPos() { return pos; }
private:
	glm::vec3 pos;
};
class VertexColor{
public:
	VertexColor(){}
	VertexColor(const glm::vec3& color){
		this->color= color;
	}
	glm::vec3 GetPos() { return color; }
private:
	glm::vec3 color;
};
class VertexData
{
public:
	VertexData(const Vertex &pos,const VertexColor &color)
	{
		this->pos = pos;
		this->color = color;
	}

	Vertex GetPos() { return pos; }
	VertexColor GetCol() { return color; }

private:
	Vertex pos;
	VertexColor color;
};

enum MeshBufferPositions
{
	POSITION_VB,
	COLOR_VB,
	NORMAL_VB,
	INDEX_VB
};
class Mesh
{
public:
   	//Mesh(const std::string& fileName);
	Mesh(Vertex* vertices, unsigned int numVertices);
	void Draw();
	void DrawLine();
	void DrawPoints();
	virtual ~Mesh();
protected:
private:
	static const unsigned int NUM_BUFFERS = 1;
	void operator=(const Mesh& mesh) {}
	Mesh(const Mesh& mesh) {}

    	//void InitMesh(const IndexedModel& model);
	int m_drawCount;
	GLuint m_vertexArrayObject;
	GLuint m_vertexArrayBuffers[NUM_BUFFERS];
	//unsigned int m_numIndices;
};
#endif
