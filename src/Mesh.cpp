#include "Mesh.h"
#include <GL/glew.h>
#include <vector>
using namespace std;
Mesh::Mesh(Vertex* vertices, unsigned int numVertices)
{
	///vector<Vertex> pos;
	//vector<VertexColor> color;
	m_drawCount = numVertices;
	//pos.reserve(numVertices);
	//color.reserve(numVertices);

	///for(int i=0;i<numVertices;i++){
//		pos.push_back(vertices[0].GetPos());
//		color.push_back(vertices[0].GetCol());
//	}
	glGenVertexArrays(1,&m_vertexArrayObject);
	glBindVertexArray(m_vertexArrayObject);

	// Number of Buffer Data to be Generated-----------------------
	glGenBuffers(NUM_BUFFERS,m_vertexArrayBuffers);
	// Vertex Drawing for Arrays
	glBindBuffer(GL_ARRAY_BUFFER,m_vertexArrayBuffers[POSITION_VB]);
	glBufferData(GL_ARRAY_BUFFER,numVertices*sizeof(vertices[0]),vertices,GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,NULL);
	glBindVertexArray(0);
	//------------------------------------------------------------------------------
	// Color Shading for Array Buffer
	/*
	glBindBuffer(GL_ARRAY_BUFFER,m_vertexArrayBuffers[COLOR_VB]);
	glBufferData(GL_ARRAY_BUFFER,numVertices*sizeof(VColor[0]),VColor,GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,NULL);
	glBindVertexArray(1);
	*/
	//------------------------------------------------------------------------------
}
Mesh::~Mesh(){
	glDeleteVertexArrays(1,&m_vertexArrayObject);
}
void Mesh::Draw(){
	glBindVertexArray(m_vertexArrayObject);
	glDrawArrays(GL_TRIANGLES,0,m_drawCount);
	glBindVertexArray(0);
}
void Mesh::DrawLine(){
	glBindVertexArray(m_vertexArrayObject);
	glDrawArrays(GL_LINE_STRIP,0,m_drawCount);
	glBindVertexArray(0);
}
void Mesh::DrawPoints(){
	glBindVertexArray(m_vertexArrayObject);
	glDrawArrays(GL_POINT,0,m_drawCount);
	glBindVertexArray(0);
}
