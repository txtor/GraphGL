#include "Display.h"
#include <iostream>


Display::Display(int width,int height,const char* title,float r,float g,float b,float a){
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
	glfwWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);
	m_window = glfwCreateWindow(width,height,title,nullptr,nullptr);
	if(m_window == NULL){
		std::cout<<"Failed to create GLFW Window"<<std::endl;
		glfwTerminate();
	}
	glfwMakeContextCurrent(m_window);
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}
void Display::bgColorSet(){
	glClearColor(r,g,b,a);
	glClear(GL_COLOR_BUFFER_BIT);
}
Display::~Display(){
	glfwDestroyWindow(m_window);
}
