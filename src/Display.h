#ifndef DISPLAY_H
#define DISPLAY_H
#include<GLFW/glfw3.h>
class Display{
public:
	Display(int width,int height,const char* title,float r = 0,float g=0,float b =0,float a=1);
	GLFWwindow *retWin(){
		return m_window;
	}
	void bgColorSet();
	virtual ~Display();
protected:
private:
	GLFWwindow *m_window;
	float r,g,b,a;
};
#endif
