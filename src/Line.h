#ifndef LINE_H
#define LINE_H
typedef unsigned int GLuint;
#include "Mesh.h"

class Line
{
public:
   	//Mesh(const std::string& fileName);
	Line(Vertex* vertices);
	void Draw();
	virtual ~Line();
protected:
private:
	
	void operator=(const Line& Line) {}
	Line(const Line& L) {}

    	//void InitMesh(const IndexedModel& model);
	
	unsigned int m_numIndices=2;
};
#endif
