#ifndef SHADERS_H
#define SHADERS_H
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
typedef unsigned int GLuint;
class Shader{
public:
	Shader(const std::string,const std::string);
	void Bind();
	//void Update(const Transform& transform, const Camera& camera);
	void colorUniform(const glm::vec3&);
	void Translate(const glm::mat4& trans);
	virtual ~Shader();
protected:
private:
	static const unsigned int NUM_SHADERS=2;
	static const unsigned int NUM_UNIFORMS = 3;
	//Shader Copy
	Shader(const Shader& other) {}
	void operator=(const Shader& other) {}

	// Shader Operations Start
	std::string LoadShader(const std::string& fileName);
	void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage);
	GLuint CreateShader(const std::string& text, unsigned int type);
	// Shader Operations End
	
	GLuint m_program;
	GLuint m_shaders[NUM_SHADERS];
	GLuint m_uniforms[NUM_UNIFORMS];
};
#endif
