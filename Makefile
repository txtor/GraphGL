EFLAGS= -g -Isrc -lglfw -lGL -lGLEW -lpthread
FILES= src/Display.cpp src/Shaders.cpp src/Mesh.cpp main.cpp

default:
	g++ $(EFLAGS) $(FILES)
clean:
	rm a.out
