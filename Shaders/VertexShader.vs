#version 330
attribute vec3 position;
uniform vec3 uniColor;
uniform mat4 mvp;
varying vec3 color;
void main()
{
	gl_Position = mvp*vec4(position,1.0);
	color = uniColor;
}
