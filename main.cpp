#include <GL/glew.h>
#include "src/Shaders.h"
#include "src/Display.h"
#include "src/Mesh.h"
#include <glm/ext/matrix_transform.hpp>
#include <glm/fwd.hpp>
#include <iostream>

int main(){
{
	Display d(800,600,"GLGraph",0.2,0.2,0.2);
	Shader s("Shaders/VertexShader.vs","Shaders/FragmentShader.fs");
	glm::mat4 trans = glm::mat4(1.0f);
	Vertex vertices[]={
		Vertex(glm::vec3(-0.5,0.0f,1.0f)),
		Vertex(glm::vec3(0,0.5,1)),
		Vertex(glm::vec3(0.5,0.0f,1)),
		Vertex(glm::vec3(1.0f,1.0f,1.0f))
	};
	Vertex vert2[]={
		Vertex(glm::vec3(-0.5,0.0,1)),
		Vertex(glm::vec3(0.0,0.0,1.0)),
		Vertex(glm::vec3(1.0,0.5,1.0))
	};
	VertexColor VColor[]={
		VertexColor(glm::vec3(1.0,0.0,0.0)),
		VertexColor(glm::vec3(0.0,1.0,0.0)),
		VertexColor(glm::vec3(0.0,0.0,1.0))
	};
	Mesh m(vertices,4);
	Mesh m1(vert2,3);
	int c=0;
		while(!glfwWindowShouldClose(d.retWin())){
		d.bgColorSet();
		s.Bind();
		s.colorUniform(VColor[0].GetPos());
		s.Translate(glm::translate(trans,glm::vec3(-0.25f,0.25f,0.0f)));
		m1.DrawLine();
		s.colorUniform(VColor[1].GetPos());
		s.Translate(glm::translate(trans,glm::vec3(0.25,-0.25f,0.0f)));
		m.DrawLine();
		glfwSwapBuffers(d.retWin());
		glfwPollEvents();
		//c++;
	}
}
	glfwTerminate();
	return 0;
}
